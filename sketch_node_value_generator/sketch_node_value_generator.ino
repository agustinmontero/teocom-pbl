static const int nodes[] = {4854, 1704, 1313, 1234, 1095, 1996, 7777, 2216, 1, 6969, 6, 5511, 2127, 6753, 2218, 4465, 7878};
static const int node_count = 17;
static const int MGE_LEN = 20;


void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(0));

}

void loop() {
  for (int n = 0; n < node_count; n++) {
    delay(2000);
    int randNumber = random(15, 40);
    char tbs[MGE_LEN];
    sprintf(tbs, "{\"%d\": %d}", nodes[n],randNumber);
    Serial.println(tbs);
  }

}
