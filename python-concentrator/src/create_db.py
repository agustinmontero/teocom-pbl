import psycopg2
from config.constants import DB_NAME, DB_PASSWORD, DB_USER, DB_HOST, DB_PORT


if __name__ == '__main__':
    conn = psycopg2.connect(host=DB_HOST, database=DB_NAME, user=DB_USER, password=DB_PASSWORD, port=DB_PORT)
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS nodes
    (node_number INTEGER PRIMARY KEY ,
     node_name VARCHAR(100) NOT NULL );''')
    conn.commit()
    cursor.execute('''CREATE TABLE IF NOT EXISTS temperature
    (temperature_id SERIAL PRIMARY KEY,
    temperature REAL NOT NULL,
    time_stamp TIMESTAMP,
     node_id INTEGER NOT NULL,
        FOREIGN KEY (node_id) REFERENCES nodes(node_number));''')
    conn.commit()
    conn.close()

