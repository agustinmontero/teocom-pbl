import models.serial_comm as sc
from workers.serial_reader import SerialReader


if __name__ == '__main__':
    serial_reader = SerialReader()
    try:
        serial_reader.run()
    except KeyboardInterrupt:
        sc.SerialComm.close_ser()
        serial_reader.close_db()
    print('Exiting...')

