import datetime as dt
import json
from time import sleep

import psycopg2
import psycopg2.extras

import models.serial_comm as sc
from config.constants import DB_NAME, DB_HOST, DB_USER, DB_PASSWORD, DB_PORT, MAX_SAMPLE_COUNT


class SerialReader:

    def __init__(self):
        self.__conn = psycopg2.connect(host=DB_HOST, database=DB_NAME, user=DB_USER, password=DB_PASSWORD, port=DB_PORT)
        self.__cursor = self.__conn.cursor()

    def run(self):
        sample_count = 0
        arg_list = []
        sql = "INSERT INTO temperature(temperature, time_stamp, node_id) VALUES (%s, %s, %s);"
        while True:
            sleep(0.1)
            s = sc.SerialComm.ser_read_line()
            if s:
                try:
                    j = json.loads(s)
                except (json.JSONDecodeError, UnicodeDecodeError):
                    print('Error converting string to json')
                    continue
                if self.check_json(j):
                    print('Recibido con exito: {}'.format(j))
                    sample_count += 1
                    node = list(j.keys())[0]
                    arg_list.append((j[node], dt.datetime.now(), node))
                    if sample_count == MAX_SAMPLE_COUNT:
                        try:
                            psycopg2.extras.execute_batch(self.__cursor, sql, arg_list)
                            self.__conn.commit()
                        except psycopg2.DatabaseError as e:
                            print('Database error: {}'.format(e))
                        finally:
                            sample_count = 0
                            arg_list = []
                else:
                    print('Invalid data format: {}'.format(j))

    @staticmethod
    def check_json(d: dict):
        from config.constants import VALID_NODES
        k_list = list(d.keys())
        if len(k_list) == 1:
            k = k_list[0]
            val = d[k]
            if isinstance(k, str) and isinstance(val, (int, float)):
                if k in VALID_NODES:
                    return True
                return False
            else:
                return False
        else:
            return False

    def close_db(self):
        if self.__conn:
            self.__conn.close()
