import psycopg2

from config.constants import VALID_NODES, DB_NAME, DB_PORT, DB_PASSWORD, DB_USER, DB_HOST

if __name__ == '__main__':
    conn = psycopg2.connect(host=DB_HOST, database=DB_NAME, user=DB_USER, password=DB_PASSWORD, port=DB_PORT)
    cursor = conn.cursor()
    node_records = []
    for key, value in VALID_NODES.items():
        node_records.append((key, value))
    try:
        cursor.executemany('''INSERT INTO nodes (node_number, node_name)
                VALUES(%s, %s) ON CONFLICT (node_number) DO UPDATE SET node_number = EXCLUDED.node_number, 
                node_name = EXCLUDED.node_name; ''', node_records)
    except psycopg2.DatabaseError as e:
        print('error en comando: {}'.format(e))
    cursor.execute('''SELECT * FROM nodes
    ;''')
    print(cursor.fetchall())
    conn.commit()
    conn.close()
