B_RATE = 9600
# PORT = '/dev/ttyACM0'
PORT = '/dev/ttyUSB0'
SER_TIMEOUT = 7

# DB_NAME = pathlib.Path('db/teocom-pbl.db')
DB_NAME = 'teocom'
DB_USER = 'teocom'
DB_PASSWORD = 'teocompbl'
DB_HOST = '192.81.219.250'
DB_PORT = 37945
DB_LOCALHOST = '192.168.201.120'
DB_LOCALPORT = 5432

MAX_SAMPLE_COUNT = 15

VALID_NODES = {
    "4854": "Computación mata electrónica",
    "1704": "Emilia4Ever",
    "1313": "Braquis-J3",
    "1234": "Electrones de Laplace",
    "1095": "Vikings PANAMA",
    "1996": "Las pociones",
    "7777": "TC2000",
    "2216": "Ca(sas+pelo)",
    "1": "Tesla",
    "6969": "Zanos",
    "6": "La constante L(v6)",
    "5511": "Los 5-1",
    "2127": "EleCom",
    "6753": "Regedit",
    "2218": "Enigma",
    "4465": "Modem de 4 patas",
    "7878": "J9",
    "7064": "Electrocomp"
}
